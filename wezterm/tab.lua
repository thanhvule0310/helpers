local wezterm = require("wezterm")
local colors = require("theme").get_colors()

local Tab = {}

local function get_process(tab)
	local process_icons = {
		["ping"] = {
			{ Foreground = { Color = colors.green } },
			{ Text = "󱠡" },
		},
		["docker"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "󰡨" },
		},
		["docker-compose"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "󰡨" },
		},
		["nvim"] = {
			{ Foreground = { Color = colors.green } },
			{ Text = "" },
		},
		["bob"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "" },
		},
		["vi"] = {
			{ Foreground = { Color = colors.green } },
			{ Text = "" },
		},
		["vim"] = {
			{ Foreground = { Color = colors.green } },
			{ Text = "" },
		},
		["node"] = {
			{ Foreground = { Color = colors.green } },
			{ Text = "󰋘" },
		},
		["unzip"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "󰗄" },
		},
		["zsh"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "" },
		},
		["bash"] = {
			{ Foreground = { Color = colors.overlay1 } },
			{ Text = "" },
		},
		["htop"] = {
			{ Foreground = { Color = colors.green } },
			{ Text = "󱕍" },
		},
		["btop"] = {
			{ Foreground = { Color = colors.rosewater } },
			{ Text = "󱕍" },
		},
		["btm"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "󱕍" },
		},
		["cargo"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "󱘗" },
		},
		["rustup"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "󱘗" },
		},
		["go"] = {
			{ Foreground = { Color = colors.sapphire } },
			{ Text = "" },
		},
		["git"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "󰊢" },
		},
		["lazygit"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "󰊢" },
		},
		["lua"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "" },
		},
		["wget"] = {
			{ Foreground = { Color = colors.yellow } },
			{ Text = "󰄠" },
		},
		["curl"] = {
			{ Foreground = { Color = colors.yellow } },
			{ Text = "" },
		},
		["gh"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "" },
		},
		["flatpak"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "󰏖" },
		},
		["paru"] = {
			{ Foreground = { Color = colors.yellow } },
			{ Text = "󰮯" },
		},
		["yay"] = {
			{ Foreground = { Color = colors.yellow } },
			{ Text = "󰮯" },
		},
		["rate-mirrors"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "󰦄" },
		},
		["fish"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "" },
		},
		["hyperfine"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "󰓅" },
		},
		["http"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "󰿡" },
		},
		["find"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "󰱼" },
		},
		["fd"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "󰱼" },
		},
		["rg"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "󱉶" },
		},
		["grep"] = {
			{ Foreground = { Color = colors.blue } },
			{ Text = "󱉶" },
		},
		["bat"] = {
			{ Foreground = { Color = colors.mauve } },
			{ Text = "󰭟" },
		},
		["cat"] = {
			{ Foreground = { Color = colors.yellow } },
			{ Text = "󰄛" },
		},
		["make"] = {
			{ Foreground = { Color = colors.peach } },
			{ Text = "" },
		},
	}

	local process_name = string.gsub(tab.active_pane.foreground_process_name, "(.*[/\\])(.*)", "%2")

	if process_icons[process_name] then
		return wezterm.format(process_icons[process_name])
	elseif process_name == "" then
		return wezterm.format({
			{ Foreground = { Color = colors.red } },
			{ Text = "󰌾" },
		})
	else
		return wezterm.format({
			{ Foreground = { Color = colors.blue } },
			{ Text = string.format("[%s]", process_name) },
		})
	end
end

local function get_current_working_dir(tab)
	local cwd_uri = tab.active_pane.current_working_dir

	if cwd_uri then
		local cwd = ""
		cwd_uri = cwd_uri:sub(8)
		local slash = cwd_uri:find("/")
		if slash then
			cwd = cwd_uri:sub(slash):gsub("%%(%x%x)", function(hex)
				return string.char(tonumber(hex, 16))
			end)
		end

		if cwd == wezterm.home_dir then
			cwd = "~"
		end

		return string.match(cwd, "[^/]+$")
	end
end

function Tab.setup(config)
	config.tab_bar_at_bottom = true
	config.use_fancy_tab_bar = false
	config.show_new_tab_button_in_tab_bar = true
	config.tab_max_width = 50
	config.hide_tab_bar_if_only_one_tab = true

	wezterm.on("format-tab-title", function(tab)
		return wezterm.format({
			{ Text = "  " },
			{ Attribute = { Intensity = "Half" } },
			{ Text = string.format("%s", tab.tab_index + 1) },
			{ Text = " " },
			"ResetAttributes",
			{ Text = get_process(tab) },
			{ Text = " " },
			{ Text = get_current_working_dir(tab) },
			{ Foreground = { Color = colors.base } },
			{ Text = " ▕" },
		})
	end)
end

return Tab
