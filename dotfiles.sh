#!/bin/bash

shopt -s extglob dotglob
case "$1" in
--sync)
	echo -e "\\033[0;34m\\033[1m ❖\\033[0m\\033[1m Syncing"
	rm -rf !(.git|.github|utils|.gitignore|dotfiles.sh|.|..)

	cp -r ~/.config/alacritty .
	cp -r ~/.config/wezterm .
	cp -r ~/.config/kitty .
	cp -r ~/.config/despell .
	cp -r ~/.config/lazygit .
	cp -r ~/.config/nvim .
	cp -r ~/.config/bat .
	cp -r ~/.config/lsd .
	cp -r ~/.config/fish .
	cp -r ~/.config/htop .
	cp -r ~/.scripts .
	cp ~/.config/starship.toml .
	cp ~/.gitconfig .
	cp ~/.tmux.conf .
	cp ~/.zshrc .
	cp -r ~/.local/share/fonts .

	cp -r ~/.config/zathura .
	cp -r ~/.config/gtk-3.0 .
	cp -r ~/.config/fontconfig .
	cp -r ~/.config/paru .
	cp ~/.Xresources .
	cp ~/.xinitrc .
	cp -r ~/.config/dunst .
	cp -r ~/.config/redshift .
	cp -r ~/.config/polybar .
	cp -r ~/.config/rofi .
	cp -r ~/.config/gtk-3.0 .
	cp -r ~/.config/zathura .
	cp -r ~/.config/i3 .
	cp -r ~/.config/picom .

	echo -e "\\033[0;32m\\033[1m ✓\\033[0m\\033[1m Done"
	;;
--restore)
	echo -e "\\033[0;34m\\033[1m ❖\\033[0m\\033[1m Set environment variables"
	sudo tee -a /etc/environment <<END
GTK_IM_MODULE="ibus"
QT_IM_MODULE="ibus"
XMODIFIERS="@im=ibus"
QT4_IM_MODULE="ibus"
CLUTTER_IM_MODULE="ibus"
GLFW_IM_MODULE="ibus"
FREETYPE_PROPERTIES="cff:no-stem-darkening=0"
END
	echo -e "\\033[0;32m\\033[1m ✓\\033[0m\\033[1m Done"

	echo -e "\\033[0;34m\\033[1m ❖\\033[0m\\033[1m Config touchpad"
	sudo tee -a /etc/X11/xorg.conf.d/30-touchpad.conf <<END
Section "InputClass"
    Identifier "devname"
    Driver "libinput"
    Option "Tapping" "on"
    Option "NaturalScrolling" "true"
EndSection
END
	echo -e "\\033[0;32m\\033[1m ✓\\033[0m\\033[1m Done"

	echo -e "\\033[0;34m\\033[1m ❖\\033[0m\\033[1m Install necessary packages"
	sudo pacman -Syyu
	paru -S --skipreview --assume-installed base-devel cmake unzip unrar p7zip ninja libvips librsvg libappindicator-gtk3 gtk3 appmenu-gtk-module openssl file webkit2gtk curl less grub-btrfs man-db neofetch fish zsh pacman-contrib zoxide vim linux-headers seahorse timeshift timeshift-autosnap htop acpid jq acpi ripgrep starship tokei fd bat rate-mirrors-bin mutter-dynamic-buffering apple_cursor bob hadolint-bin actionlint-bin taplo-cli fnm-bin python-pip lua lua-language-server luajit visual-studio-code-bin postman-bin docker docker-buildx docker-compose kitty wezterm alacritty github-cli dbeaver shellcheck shfmt stylua mongodb-compass-beta-bin hyperfine tmux diff-so-fancy lazygit checkmake go xf86-video-intel vulkan-intel apple-fonts noto-fonts noto-fonts-cjk noto-fonts-extra ttf-apple-emoji ttf-cascadia-code-nerd realtime-privileges tuned easyeffects zam-plugins calf lsp-plugins cloudflare-warp-bin popcorntime-bin firefox-developer-edition google-chrome ibus-bamboo-git microsoft-edge-dev-bin obsidian trashy mesa mesa-utils qpwgraph lxappearance-gtk3 picom pavucontrol xorg-apps maim xautolock i3-wm i3exit autotiling xfce-polkit polybar lm_sensors acpid jq redshift inotify-tools xdotool acpi xclip papirus-icon-theme rofi dunst nitrogen qbittorrent materia-gtk-theme thunar thunar-media-tags-plugin gvfs libimobiledevice usbmuxd zathura zathura-pdf-mupdf

	sudo usermod -aG docker $USER
	sudo systemctl enable --now docker.service
	sudo systemctl enable --now containerd.service
	sudo systemctl enable --now acpid.service
	sudo systemctl enable --now tuned.service
	sudo tuned-adm profile latency-performance
	sudo groupadd realtime
	sudo usermod -a -G realtime $USER

	sudo tuned-adm profile latency-performance
	sudo tuned-adm active

	sudo cp ./utils/files/20-intel.conf /etc/X11/xorg.conf.d/

	echo -e "\\033[0;34m\\033[1m ❖\\033[0m\\033[1m Install udev"
	sudo cp ./utils/files/udev/* /etc/udev/rules.d/
	echo -e "\\033[0;32m\\033[1m ✓\\033[0m\\033[1m Done"

	cp -r ./.scripts ~/
	cp -r ./alacritty ~/.config
	cp -r ./wezterm ~/.config
	cp -r ./kitty ~/.config
	cp -r ./despell ~/.config
	cp -r ./lazygit ~/.config
	cp -r ./fish ~/.config
	cp -r ./htop ~/.config
	cp -r ./lsd ~/.config
	cp -r ./bat ~/.config
	cp -r ./nvim ~/.config
	cp ./.gitconfig ~/
	cp ./.tmux.conf ~/
	cp ./starship.toml ~/.config
	cp -r fonts ~/.local/share/

	cp -rv ./paru ~/.config

	cp -r ./fontconfig ~/.config
	cp -r ./dunst ~/.config
	cp -r ./polybar ~/.config
	cp -r ./rofi ~/.config
	cp ./.Xresources ~/
	cp ./.xinitrc ~/
	cp -r ./gtk-3.0 ~/.config
	cp -r ./zathura ~/.config
	cp -r ./i3 ~/.config
	cp -r ./picom ~/.config
	cp -r ./redshift ~/.config

	echo -e "\\033[0;34m\\033[1m ❖\\033[0m\\033[1m Enhance Audio"
	sudo cp ./utils/files/increase_rtc /usr/bin/
	sudo cp ./utils/files/increase_rtc.service /etc/systemd/system/
	sudo systemctl enable --now increase_rtc.service

	sudo tee /etc/sysctl.d/90-swappiness.conf <<END
vm.swappiness = 10
END

	sudo tee /etc/sysctl.d/90-max_user_watches.conf <<END
fs.inotify.max_user_watches = 600000
END

	sudo setpci -v -d *:* latency_timer=b0

	sudo grub-mkconfig -o /boot/grub/grub.cfg
	echo -e "\\033[0;32m\\033[1m ✓\\033[0m\\033[1m Done"

	echo -e "\\033[0;32m\\033[1m ✓\\033[0m\\033[1m Restored"

	mkdir -p ~/Workspace/{Projects/taki,Sandbox,CS,Repos}
	;;

*)
	echo default
	;;
esac
