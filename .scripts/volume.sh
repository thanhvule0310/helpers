#!/bin/bash

case $1 in
--up)
	if [ $(pactl list sinks | grep '^[[:space:]]Volume:' | head -n 1 | awk '{print $5}' | cut -d'%' -f1) -ge 95 ]; then
		pactl set-sink-volume @DEFAULT_SINK@ 100%
	else
		pactl set-sink-volume @DEFAULT_SINK@ +5%
	fi
	notify-send --icon=audio-speakers-symbolic -r 99 "VOLUME: $(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(($SINK + 1)) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')%"
	;;
--down)
	pactl set-sink-volume @DEFAULT_SINK@ -5%
	notify-send --icon=audio-speakers-symbolic -r 99 "VOLUME: $(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(($SINK + 1)) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')%"
	;;
--toggle)
	pactl set-sink-mute @DEFAULT_SINK@ toggle

	if pactl list sinks | grep -q "Mute: yes"; then
		notify-send --icon=audio-volume-muted-symbolic -r 99 "MUTED"
	else
		notify-send --icon=audio-speakers-symbolic -r 99 "UNMUTED"
	fi
	;;
*) ;;
esac
