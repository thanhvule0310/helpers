#!/bin/bash
current_ibus_engine=$(ibus engine)

ENGLISH="xkb:us::eng"
VIETNAM="Bamboo"

case $1 in
--toggle)
	if [[ "$current_ibus_engine" == "$ENGLISH" ]]; then
		ibus engine $VIETNAM
		polybar-msg hook ibus 1 &>/dev/null || true
		notify-send -r 100 -i "input-keyboard-symbolic" "VN"
	else
		ibus engine $ENGLISH
		polybar-msg hook ibus 1 &>/dev/null || true
		notify-send -r 100 -i "input-keyboard-symbolic" "EN"
	fi
	/usr/bin/sudo -u thanhvule0310 /usr/bin/paplay --server=/run/user/1000/pulse/native /home/thanhvule0310/.scripts/ibus.wav >/dev/null 2>&1
	;;
*)
	if [[ "$current_ibus_engine" == "$VIETNAM" ]]; then
		echo "VN"
	else
		echo "EN"
	fi
	;;
esac
