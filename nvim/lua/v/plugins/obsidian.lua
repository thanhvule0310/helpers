return {
    "epwalsh/obsidian.nvim",
    requires = {
        "nvim-lua/plenary.nvim",
    },
    event = {
        "BufReadPre " .. vim.fn.expand("~") .. "/Workspace/Repos/notes/**.md",
        "BufNewFile " .. vim.fn.expand("~") .. "/Workspace/Repos/notes/**.md",
    },
    cmd = { "ObsidianNew" },
    keys = {
        { "<leader>ot", ":ObsidianToday<cr>", desc = "󰸗 Open today note" },
        { "<leader>oy", ":ObsidianYesterday<cr>", desc = "󰃰 Open yesterday note" },
        { "<leader>os", ":ObsidianSearch<cr>", desc = "󱎸 Search" },
        { "<leader>of", ":ObsidianQuickSwitch<cr>", desc = "󰱼 Quick File Switch" },
        { "<leader>oo", ":ObsidianFollowLink<cr>", desc = "󰃷 Follow Link" },
    },
    opts = {
        disable_frontmatter = true,
        workspaces = {
            {
                name = "Notes",
                path = "~/Workspace/Repos/notes",
            },
        },
        templates = {
            subdir = "_templates",
            date_format = "%d-%m-%Y",
            time_format = "%H:%M",
            substitutions = {},
        },
        daily_notes = {
            folder = "Dailies",
            date_format = "%d-%m-%Y",
            alias_format = "%B %-d, %Y",
            template = "_daily.md",
        },
        mappings = {},
    },
}
