return {
    "neovim/nvim-lspconfig",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
        "b0o/schemastore.nvim",
        "hrsh7th/cmp-nvim-lsp",
        "creativenull/efmls-configs-nvim",
        "folke/neodev.nvim",
    },
    opts = {
        inlay_hints = { enabled = true },
    },
    config = function()
        local lspconfig = require("lspconfig")

        vim.fn.sign_define("DiagnosticSignError", { text = "󰅙", texthl = "DiagnosticSignError", numhl = "DiagnosticSignError" })
        vim.fn.sign_define("DiagnosticSignHint", { text = "󰠠", texthl = "DiagnosticSignHint", numhl = "DiagnosticSignHint" })
        vim.fn.sign_define("DiagnosticSignInfo", { text = "󰸥", texthl = "DiagnosticSignInfo", numhl = "DiagnosticSignInfo" })
        vim.fn.sign_define("DiagnosticSignWarn", { text = "󰀨", texthl = "DiagnosticSignWarn", numhl = "DiagnosticSignWarn" })

        vim.diagnostic.config({
            virtual_text = {
                source = false,
                prefix = "",
                format = function(diagnostic)
                    local sign = "󱡝"

                    if diagnostic.severity == vim.diagnostic.severity.ERROR then
                        sign = vim.fn.sign_getdefined("DiagnosticSignError")[1].text
                    elseif diagnostic.severity == vim.diagnostic.severity.WARN then
                        sign = vim.fn.sign_getdefined("DiagnosticSignWarn")[1].text
                    elseif diagnostic.severity == vim.diagnostic.severity.INFO then
                        sign = vim.fn.sign_getdefined("DiagnosticSignInfo")[1].text
                    elseif diagnostic.severity == vim.diagnostic.severity.HINT then
                        sign = vim.fn.sign_getdefined("DiagnosticSignHint")[1].text
                    end

                    return string.format("%s%s: %s ", sign, diagnostic.source or "", diagnostic.message)
                end,
            },
            float = {
                source = false,
                border = "single",
                header = false,
                format = function(diagnostic)
                    return string.format("%s: %s ", diagnostic.source or "", diagnostic.message)
                end,
            },
            signs = true,
            underline = true,
            update_in_insert = false,
            severity_sort = true,
        })

        vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
            border = "single",
        })

        vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
            border = "single",
        })

        require("lspconfig.ui.windows").default_options = {
            border = "single",
        }

        local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
        local on_attach = function(client, bufnr)
            if client.supports_method("textDocument/formatting") then
                vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
                vim.api.nvim_create_autocmd("BufWritePre", {
                    group = augroup,
                    buffer = bufnr,
                    callback = function()
                        vim.lsp.buf.format({ timeout = 5000, bufnr = bufnr })
                    end,
                })
            end
        end

        vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Next Diagnostic" })
        vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Previous Diagnostic" })
        vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Next Diagnostic" })

        vim.api.nvim_create_autocmd("LspAttach", {
            group = vim.api.nvim_create_augroup("UserLspConfig", {}),
            callback = function(ev)
                vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, { buffer = ev.buf, desc = "Signature Help" })
                vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, { buffer = ev.buf, desc = "Show Type Definition" })
                vim.keymap.set("n", "<leader>lc", vim.lsp.buf.code_action, { buffer = ev.buf, desc = "Code Actions" })
                vim.keymap.set("n", "<leader>lf", function()
                    vim.lsp.buf.format({ timeout = 5000 })
                end, { buffer = ev.buf, desc = "Format current file" })
                vim.keymap.set("n", "<leader>lr", vim.lsp.buf.rename, { buffer = ev.buf, desc = "Rename" })
                vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = ev.buf, desc = "Show hover" })
                vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { buffer = ev.buf, desc = "Goto Declaration" })
                vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = ev.buf, desc = "Goto Definition" })
                vim.keymap.set("n", "gi", vim.lsp.buf.implementation, { buffer = ev.buf, desc = "Goto Implementation" })

                if vim.lsp.inlay_hint then
                    vim.keymap.set("n", "<leader>li", function()
                        vim.lsp.inlay_hint(0, nil)
                    end, { buffer = ev.buf, desc = "Toggle Inlay Hints" })
                end
            end,
        })

        local capabilities =
            vim.tbl_deep_extend("force", vim.lsp.protocol.make_client_capabilities(), require("cmp_nvim_lsp").default_capabilities())

        lspconfig.bashls.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            cmd_env = {
                GLOB_PATTERN = "*@(.sh|.inc|.bash|.command|.zsh)",
            },
            filetypes = { "sh", "zsh" },
        })

        lspconfig.cssls.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })

        lspconfig.docker_compose_language_service.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })

        lspconfig.emmet_language_server.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })

        lspconfig.eslint.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            root_dir = lspconfig.util.root_pattern(
                ".eslintrc",
                ".eslintrc.js",
                ".eslintrc.yml",
                ".eslintrc.json",
                "eslint.config.js",
                ".eslintrc.cjs",
                ".eslintrc.yaml"
            ),
        })

        lspconfig.golangci_lint_ls.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })

        lspconfig.gopls.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            settings = {
                gopls = {
                    experimentalPostfixCompletions = true,
                    analyses = {
                        unusedparams = true,
                        shadow = true,
                    },
                    staticcheck = true,
                },
            },
            init_options = {
                usePlaceholders = true,
            },
        })

        lspconfig.graphql.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })

        lspconfig.html.setup({
            on_attach = function(client, bufnr)
                client.server_capabilities.documentFormattingProvider = false
                client.server_capabilities.documentRangeFormattingProvider = false

                on_attach(client, bufnr)
            end,
            capabilities = capabilities,
        })

        lspconfig.jsonls.setup({
            on_attach = function(client, bufnr)
                client.server_capabilities.documentFormattingProvider = false
                client.server_capabilities.documentRangeFormattingProvider = false
                on_attach(client, bufnr)
            end,
            settings = {
                json = {
                    schemas = require("schemastore").json.schemas(),
                },
            },
            init_options = {
                provideFormatter = true,
            },
            capabilities = capabilities,
        })

        require("neodev").setup({
            library = { plugins = { "nvim-dap-ui" }, types = true },
        })
        lspconfig.lua_ls.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            settings = {
                Lua = {
                    runtime = { version = "LuaJIT" },
                    diagnostics = { globals = { "vim" } },
                    format = { enable = false },
                    completion = { callSnippet = "Replace" },
                    workspace = { checkThirdParty = "Disable" },
                    telemetry = { enable = false },
                    hint = { enable = true },
                },
            },
        })

        lspconfig.rust_analyzer.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            cmd = { "rustup", "run", "stable", "rust-analyzer" },
        })

        lspconfig.taplo.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })

        lspconfig.yamlls.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            settings = {
                yaml = {
                    schemas = {
                        ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
                    },
                },
            },
        })

        lspconfig.tailwindcss.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            root_dir = lspconfig.util.root_pattern(
                "tailwind.config.js",
                "tailwind.config.cjs",
                "tailwind.config.mjs",
                "tailwind.config.ts",
                "postcss.config.js",
                "postcss.config.cjs",
                "postcss.config.mjs",
                "postcss.config.ts"
            ),
        })

        local _efm = {
            formatters = {
                fish_indent = require("efmls-configs.formatters.fish_indent"),
                prettier_d = require("efmls-configs.formatters.prettier_d"),
                shfmt = require("efmls-configs.formatters.shfmt"),
                stylua = require("efmls-configs.formatters.stylua"),
            },
            linters = {
                actionlint = require("efmls-configs.linters.actionlint"),
                fish = require("efmls-configs.linters.fish"),
                hadolint = require("efmls-configs.linters.hadolint"),
                shellcheck = require("efmls-configs.linters.shellcheck"),
                yamllint = require("efmls-configs.linters.yamllint"),
            },
        }

        lspconfig.efm.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            init_options = { documentFormatting = true },
            settings = {
                languages = {
                    ["markdown.mdx"] = { _efm.formatters.prettier_d },
                    bash = { _efm.formatters.shfmt, _efm.linters.shellcheck },
                    css = { _efm.formatters.prettier_d },
                    dockerfile = { _efm.linters.hadolint },
                    fish = { _efm.linters.fish, _efm.formatters.fish_indent },
                    graphql = { _efm.formatters.prettier_d },
                    html = { _efm.formatters.prettier_d },
                    javascript = { _efm.formatters.prettier_d },
                    javascriptreact = { _efm.formatters.prettier_d },
                    json = { _efm.formatters.prettier_d },
                    jsonc = { _efm.formatters.prettier_d },
                    less = { _efm.formatters.prettier_d },
                    lua = { _efm.formatters.stylua },
                    markdown = { _efm.formatters.prettier_d },
                    sass = { _efm.formatters.prettier_d },
                    scss = { _efm.formatters.prettier_d },
                    sh = { _efm.formatters.shfmt, _efm.linters.shellcheck },
                    typescript = { _efm.formatters.prettier_d },
                    typescriptreact = { _efm.formatters.prettier_d },
                    vue = { _efm.formatters.prettier_d },
                    yaml = { _efm.formatters.prettier_d, _efm.linters.yamllint, _efm.linters.actionlint },
                    zsh = { _efm.formatters.shfmt },
                },
            },
        })
    end,
}
