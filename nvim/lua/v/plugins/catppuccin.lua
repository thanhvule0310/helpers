return {
    "catppuccin/nvim",
    name = "catppuccin",
    lazy = false,
    priority = 1000,
    config = function()
        require("catppuccin").setup({
            background = {
                light = "latte",
                dark = "mocha",
            },
            color_overrides = {
                latte = {
                    rosewater = "#d73a49",
                    flamingo = "#d73a49",
                    red = "#d73a49",
                    maroon = "#d73a49",
                    pink = "#bf3989",
                    mauve = "#6f42c1",
                    peach = "#e36209",
                    yellow = "#9a6700",
                    green = "#22863a",
                    teal = "#1b7c83",
                    sky = "#1b7c83",
                    sapphire = "#1b7c83",
                    blue = "#005cc5",
                    lavender = "#005cc5",
                    text = "#24292e",
                    subtext1 = "#24292f",
                    subtext0 = "#32383f",
                    overlay2 = "#424a53",
                    overlay1 = "#57606a",
                    overlay0 = "#6e7781",
                    surface2 = "#8c8c8c",
                    surface1 = "#d1d1d1",
                    surface0 = "#e6e6e6",
                    base = "#FFFFFF",
                    mantle = "#f2f2f2",
                    crust = "#ebebeb",
                },
                mocha = {
                    rosewater = "#f97583",
                    flamingo = "#f97583",
                    red = "#f97583",
                    maroon = "#f97583",
                    pink = "#f778ba",
                    mauve = "#b392f0",
                    peach = "#ffab70",
                    yellow = "#d29922",
                    green = "#85e89d",
                    teal = "#76e3ea",
                    sky = "#76e3ea",
                    sapphire = "#76e3ea",
                    blue = "#79b8ff",
                    lavender = "#79b8ff",
                    text = "#e1e4e8",
                    subtext1 = "#f4f4f4",
                    subtext0 = "#f0f6fc",
                    overlay2 = "#c9d1d9",
                    overlay1 = "#b1bac4",
                    overlay0 = "#8b949e",
                    surface2 = "#6e7681",
                    surface1 = "#484f58",
                    surface0 = "#262b33",
                    base = "#0d1117",
                    mantle = "#0a0d12",
                    crust = "#000000",
                },
            },
            transparent_background = false,
            show_end_of_buffer = false,
            integration_default = false,
            integrations = {
                cmp = true,
                dropbar = { enabled = true },
                gitsigns = true,
                hop = true,
                illuminate = { enabled = true },
                markdown = true,
                native_lsp = { enabled = true, inlay_hints = { background = true } },
                neogit = true,
                neotree = true,
                semantic_tokens = true,
                treesitter = true,
                treesitter_context = true,
                vimwiki = true,
                which_key = true,
            },
            highlight_overrides = {
                all = function(colors)
                    return {
                        CmpItemMenu = { fg = colors.surface2 },
                        CursorLineNr = { fg = colors.text },
                        FloatBorder = { bg = colors.base, fg = colors.surface0 },
                        GitSignsChange = { fg = colors.peach },
                        LineNr = { fg = colors.surface1 },
                        LspInfoBorder = { link = "FloatBorder" },
                        NeoTreeDirectoryIcon = { fg = colors.overlay2 },
                        NeoTreeDirectoryName = { fg = colors.overlay2 },
                        NeoTreeFloatBorder = { link = "TelescopeResultsBorder" },
                        NeoTreeGitConflict = { fg = colors.red },
                        NeoTreeGitDeleted = { fg = colors.red },
                        NeoTreeGitIgnored = { fg = colors.overlay0 },
                        NeoTreeGitModified = { fg = colors.peach },
                        NeoTreeGitStaged = { fg = colors.green },
                        NeoTreeGitUnstaged = { fg = colors.red },
                        NeoTreeGitUntracked = { fg = colors.green },
                        NeoTreeIndent = { fg = colors.surface1 },
                        NeoTreeNormal = { bg = colors.mantle },
                        NeoTreeNormalNC = { bg = colors.mantle },
                        NeoTreeRootName = { fg = colors.subtext1, style = { "bold" } },
                        NeoTreeTabActive = { fg = colors.text, bg = colors.mantle },
                        NeoTreeTabInactive = { fg = colors.surface2, bg = colors.crust },
                        NeoTreeTabSeparatorActive = { fg = colors.mantle, bg = colors.mantle },
                        NeoTreeTabSeparatorInactive = { fg = colors.crust, bg = colors.crust },
                        NeoTreeWinSeparator = { fg = colors.base, bg = colors.base },
                        NormalFloat = { bg = colors.base },
                        Pmenu = { bg = colors.mantle, fg = "" },
                        PmenuSel = { bg = colors.surface0, fg = "" },
                        TelescopePreviewBorder = { bg = colors.crust, fg = colors.crust },
                        TelescopePreviewNormal = { bg = colors.crust },
                        TelescopePreviewTitle = { fg = colors.crust, bg = colors.crust },
                        TelescopePromptBorder = { bg = colors.surface0, fg = colors.surface0 },
                        TelescopePromptCounter = { fg = colors.mauve, style = { "bold" } },
                        TelescopePromptNormal = { bg = colors.surface0 },
                        TelescopePromptPrefix = { bg = colors.surface0 },
                        TelescopePromptTitle = { fg = colors.surface0, bg = colors.surface0 },
                        TelescopeResultsBorder = { bg = colors.mantle, fg = colors.mantle },
                        TelescopeResultsNormal = { bg = colors.mantle },
                        TelescopeResultsTitle = { fg = colors.mantle, bg = colors.mantle },
                        TelescopeSelection = { bg = colors.surface0 },
                        VertSplit = { bg = colors.base, fg = colors.surface0 },
                        WhichKeyFloat = { bg = colors.mantle },
                        YankHighlight = { bg = colors.surface2 },
                        FidgetTask = { fg = colors.subtext2 },
                        FidgetTitle = { fg = colors.peach },

                        IblIndent = { fg = colors.surface0 },
                        IblScope = { fg = colors.overlay0 },

                        DropBarIconUISeparator = { fg = colors.surface2 },
                        DropBarIconKindFolder = { link = "NeoTreeDirectoryIcon" },

                        ["@text.title.1.markdown"] = { fg = colors.red, style = { "bold" } },
                        ["@text.title.2.markdown"] = { fg = colors.peach, style = { "bold" } },
                        ["@text.title.3.markdown"] = { fg = colors.yellow, style = { "bold" } },
                        ["@text.title.4.markdown"] = { fg = colors.green, style = { "bold" } },
                        ["@text.title.5.markdown"] = { fg = colors.mauve, style = { "bold" } },
                        ["@text.title.6.markdown"] = { fg = colors.sky, style = { "bold" } },

                        ["@parameter.bash"] = { fg = colors.text },
                    }
                end,
                latte = function(colors)
                    return {
                        LineNr = { fg = colors.surface1 },

                        IblIndent = { fg = colors.mantle },
                        IblScope = { fg = colors.surface1 },

                        String = { fg = "#032f62" },
                        Boolean = { fg = colors.blue },
                        Constant = { fg = colors.text, style = { "bold" } },
                        Float = { fg = colors.text },
                        Function = { fg = colors.mauve },
                        Keyword = { fg = colors.red },
                        Number = { fg = colors.text },
                        StorageClass = { fg = colors.mauve },
                        Structure = { fg = colors.mauve },
                        Type = { fg = colors.mauve, style = { "bold" } },
                        ["@conditional"] = { link = "Keyword" },
                        ["@constant"] = { fg = colors.blue, style = { "bold" } },
                        ["@constructor"] = { fg = colors.blue },
                        ["@function"] = { link = "Function" },
                        ["@function.builtin"] = { link = "Function" },
                        ["@function.call"] = { link = "Function" },
                        ["@function.macro"] = { link = "Function" },
                        ["@include"] = { link = "Keyword" },
                        ["@keyword"] = { link = "Keyword" },
                        ["@keyword.export"] = { fg = colors.red, style = { "bold" } },
                        ["@keyword.function"] = { link = "Keyword" },
                        ["@keyword.operator"] = { link = "Keyword" },
                        ["@keyword.return"] = { link = "Keyword" },
                        ["@method"] = { link = "Function" },
                        ["@method.call"] = { link = "Function" },
                        ["@number"] = { fg = colors.blue },
                        ["@number.css"] = { fg = colors.peach },
                        ["@operator"] = { fg = colors.red },
                        ["@operator.css"] = { fg = colors.red },
                        ["@operator.scss"] = { fg = colors.red },
                        ["@parameter"] = { fg = colors.peach },
                        ["@preproc"] = { fg = colors.surface2 },
                        ["@property"] = { fg = colors.text },
                        ["@property.class"] = { fg = colors.mauve },
                        ["@property.class.css"] = { fg = colors.mauve },
                        ["@property.class.scss"] = { fg = colors.mauve },
                        ["@property.css"] = { fg = colors.blue },
                        ["@property.id.css"] = { fg = colors.mauve },
                        ["@property.id.scss"] = { fg = colors.mauve },
                        ["@property.scss"] = { fg = colors.blue },
                        ["@repeat"] = { link = "Keyword" },
                        ["@string.plain.css"] = { fg = colors.peach },
                        ["@tag.attribute"] = { fg = colors.mauve },
                        ["@tag.html"] = { fg = colors.green },
                        ["@type"] = { link = "Type" },
                        ["@type.builtin"] = { fg = colors.mauve, style = { "bold" } },
                        ["@type.css"] = { fg = colors.green },
                        ["@type.qualifier"] = { link = "Keyword" },
                        ["@type.scss"] = { fg = colors.green },
                        ["@type.tag.css"] = { fg = colors.mauve },
                        ["@type.typescriptreact"] = { link = "Type" },
                        ["@variable"] = { fg = colors.text },
                        ["@variable.builtin"] = { fg = colors.text },
                    }
                end,
                mocha = function(colors)
                    return {
                        Boolean = { fg = colors.blue },
                        Constant = { fg = colors.text, style = { "bold" } },
                        Float = { fg = colors.text },
                        Function = { fg = colors.mauve },
                        Keyword = { fg = colors.red },
                        Number = { fg = colors.text },
                        StorageClass = { fg = colors.mauve },
                        String = { fg = "#a5d6ff" },
                        Structure = { fg = colors.mauve },
                        Type = { fg = colors.peach, style = { "bold" } },
                        ["@conditional"] = { link = "Keyword" },
                        ["@constant"] = { fg = colors.blue, style = { "bold" } },
                        ["@constructor"] = { fg = colors.blue },
                        ["@function"] = { link = "Function" },
                        ["@function.builtin"] = { link = "Function" },
                        ["@function.call"] = { link = "Function" },
                        ["@function.macro"] = { link = "Function" },
                        ["@include"] = { link = "Keyword" },
                        ["@keyword"] = { link = "Keyword" },
                        ["@keyword.export"] = { fg = colors.red, style = { "bold" } },
                        ["@keyword.function"] = { link = "Keyword" },
                        ["@keyword.operator"] = { link = "Keyword" },
                        ["@keyword.return"] = { link = "Keyword" },
                        ["@method"] = { link = "Function" },
                        ["@method.call"] = { link = "Function" },
                        ["@number"] = { fg = colors.blue },
                        ["@number.css"] = { fg = colors.peach },
                        ["@operator"] = { fg = colors.red },
                        ["@operator.css"] = { fg = colors.red },
                        ["@operator.scss"] = { fg = colors.red },
                        ["@parameter"] = { fg = colors.peach },
                        ["@preproc"] = { fg = colors.surface2 },
                        ["@property"] = { fg = colors.text },
                        ["@property.class"] = { fg = colors.mauve },
                        ["@property.class.css"] = { fg = colors.mauve },
                        ["@property.class.scss"] = { fg = colors.mauve },
                        ["@property.css"] = { fg = colors.blue },
                        ["@property.id.css"] = { fg = colors.mauve },
                        ["@property.id.scss"] = { fg = colors.mauve },
                        ["@property.scss"] = { fg = colors.blue },
                        ["@repeat"] = { link = "Keyword" },
                        ["@string.plain.css"] = { fg = colors.peach },
                        ["@tag.attribute"] = { fg = colors.mauve },
                        ["@tag.html"] = { fg = colors.green },
                        ["@type"] = { link = "Type" },
                        ["@type.builtin"] = { fg = colors.mauve, style = { "bold" } },
                        ["@type.css"] = { fg = colors.green },
                        ["@type.qualifier"] = { link = "Keyword" },
                        ["@type.scss"] = { fg = colors.green },
                        ["@type.tag.css"] = { fg = colors.mauve },
                        ["@type.typescriptreact"] = { link = "Type" },
                        ["@variable"] = { fg = colors.text },
                        ["@variable.builtin"] = { fg = colors.text },
                    }
                end,
            },
        })

        vim.api.nvim_command("colorscheme catppuccin")
    end,
}
