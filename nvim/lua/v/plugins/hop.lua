return {
    "phaazon/hop.nvim",
    cmd = { "HopWord" },
    keys = { { "H", "<cmd>HopWord<cr>", desc = "Hop Word" } },
    config = true,
}
