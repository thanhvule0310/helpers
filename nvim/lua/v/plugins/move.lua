return {
    "echasnovski/mini.move",
    event = { "BufReadPost", "BufNewFile" },
    opts = {
        mappings = {
            left = "H",
            right = "L",
            down = "J",
            up = "K",

            line_left = "",
            line_right = "",
            line_down = "",
            line_up = "",
        },
    },
}
