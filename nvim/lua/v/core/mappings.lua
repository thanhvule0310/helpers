local _map = vim.keymap.set

_map("n", "<leader>bk", "<CMD>bd<CR>", { desc = "Kill current buffer" })
_map("n", "<leader>bK", "<CMD>%bd|e#|bd#<CR>", { desc = "Kill other buffers" })
_map("n", "[b", "<CMD>bprevious<CR>", { desc = "Previous buffer" })
_map("n", "]b", "<CMD>bnext<CR>", { desc = "Next buffer" })

_map("n", "<leader>n", "<CMD>tabnew<CR>", { desc = "New tab" })
_map("n", "[t", "<CMD>tabprevious<CR>", { desc = "Previous tab" })
_map("n", "]t", "<CMD>tabnext<CR>", { desc = "Next tab" })
