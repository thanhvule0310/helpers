local _command = vim.api.nvim_create_user_command

_command("Format", function()
    vim.lsp.buf.format({ timeout = 5000 })
end, {})
