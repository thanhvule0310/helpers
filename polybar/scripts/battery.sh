#!/bin/sh

battery_print() {
	PATH_AC="/sys/class/power_supply/AC"
	PATH_BATTERY="/sys/class/power_supply/BAT0"

	ac=0
	battery_level=0
	battery_max=0

	if [ -f "$PATH_AC/online" ]; then
		ac=$(cat "$PATH_AC/online")
	fi

	if [ -f "$PATH_BATTERY/charge_now" ]; then
		battery_level=$(cat "$PATH_BATTERY/charge_now")
	fi

	if [ -f "$PATH_BATTERY/charge_full" ]; then
		battery_max=$(cat "$PATH_BATTERY/charge_full")
	fi

	battery_percent=$(("$battery_level * 100"))
	battery_percent=$(("$battery_percent / $battery_max"))

	if [ "$ac" -eq 1 ]; then
		output="$battery_percent% [AC]"
	else
		output="$battery_percent%"
	fi
	echo "$output"
}

path_pid="/tmp/polybar-battery-combined-udev.pid"

case "$1" in
--update)
	pid=$(cat $path_pid)

	if [ "$pid" != "" ]; then
		kill -10 "$pid"
	fi
	;;
*)
	echo $$ >$path_pid

	trap exit INT
	trap "echo" USR1

	while true; do
		battery_print

		sleep 1 &
		wait
	done
	;;
esac
